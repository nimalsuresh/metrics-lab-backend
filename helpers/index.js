exports.errorHandler = (condition, statusCode = 500, message = "something went wrong") => {
    if (condition) {
        throw { status: statusCode, message: message };
    }
};

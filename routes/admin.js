require("dotenv").config();
var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");

const { login, listAllUserFiles } = require("../controllers/admin");

const validateRoute = (req, res, next) => {
    const bearerHeader = req.headers["x-access-token"];

    if (bearerHeader) {
        const token = bearerHeader;

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (decoded) {
                if (decoded.type && decoded.type !== "admin") {
                    return res.status(401).send({
                        message: "unauthorized",
                    });
                }

                req.user = decoded;
                next();
            } else {
                return res.status(401).send({
                    message: "unauthorized",
                });
            }
        });
    } else {
        res.status(401).send({
            message: "unauthorized",
        });
    }
};

// For login to admin account
router.post("/login", async (req, res, next) => {
    await login(req, res);
});

router.get("/list-all-user-files", validateRoute, async (req, res, next) => {
    await listAllUserFiles(req, res);
});

module.exports = router;

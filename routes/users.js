require("dotenv").config();
var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");

const { register, login, listAllFiles, saveNewFile } = require("../controllers/users");

const validateRoute = (req, res, next) => {
    const bearerHeader = req.headers["x-access-token"];

    if (bearerHeader) {
        const token = bearerHeader;

        jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
            if (decoded) {
                if (decoded.type && decoded.type !== "user") {
                    return res.status(401).send({
                        message: "unauthorized",
                    });
                }

                req.user = decoded;
                next();
            } else {
                return res.status(401).send({
                    message: "unauthorized",
                });
            }
        });
    } else {
        res.status(401).send({
            message: "unauthorized",
        });
    }
};

/* GET users listing. */
router.get("/", function (req, res, next) {
    res.send("respond with a resource");
});

//For Registering a new user
router.post("/register", async (req, res, next) => {
    await register(req, res);
});

//For login in to user account
router.post("/login", async (req, res, next) => {
    await login(req, res);
});

router.get("/list-all-files", validateRoute, async (req, res, next) => {
    await listAllFiles(req, res);
});

router.post("/save-new-file", validateRoute, async (req, res, next) => {
    await saveNewFile(req, res);
});

module.exports = router;

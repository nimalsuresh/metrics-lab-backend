var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const mongoose = require("mongoose");
const cors = require("cors");
var bodyParser = require('body-parser')

require("dotenv").config();

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var adminRoutes = require("./routes/admin");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
// app.use(express.json());
app.use(express.urlencoded({ extended: false}));
app.use(bodyParser.json({limit:"20mb",}))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
// app.use(bodyParser.urlencoded({limit:"50mb",extended:true}))

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/admin", adminRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

connect();

function listen() {
    const port = process.env.PORT || 3000;
    app.listen(port);
    console.log("Express app started on port " + port);
}

function connect() {
    mongoose.connection.on("error", console.log).on("disconnected", connect).once("open", listen);
    return mongoose.connect(process.env.DB_URL, {
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
}

module.exports = app;

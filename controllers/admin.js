require("dotenv").config();
const jwt = require("jsonwebtoken");

const { errorHandler } = require("../helpers");
const filesCollection = require("../db-schemas/files");

exports.login = async (req, res) => {
    try {
        let { body } = req;

        errorHandler(body.email !== process.env.ADMIN_EMAIL, 400, "Incorrect admin email");

        errorHandler(body.password !== process.env.ADMIN_PASSWORD, 400, "incorrect password");

        var token = jwt.sign(
            {
                name: "admin",
                type: "admin",
            },
            process.env.JWT_SECRET,
            {
                expiresIn: "5h",
            }
        );

        return res.send({
            message: "Login success",
            token: token,
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

exports.listAllUserFiles = async (req, res) => {
    try {
        let files = await filesCollection
            .find()
            .select(["name", "file", "user_id"])
            .populate("user_id", ["name", "email"])
            .lean()
            .exec();

        return res.send({
            message: "data listed",
            data: files.length ? files : [],
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

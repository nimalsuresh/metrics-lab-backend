require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const usersCollection = require("../db-schemas/users");
const filesCollection = require("../db-schemas/files");
const { errorHandler } = require("../helpers");
const saltRounds = 10;

exports.register = async (req, res) => {
    try {
        let { body } = req;

        let user = await usersCollection.find({ email: body.email }).lean().exec();
        errorHandler(user.length, 403, "user already registered");

        let hashedPassword = await bcrypt.hash(body.password, saltRounds);

        await usersCollection.create({
            name: body.name,
            email: body.email,
            password: hashedPassword,
        });

        return res.send({
            message: "user registered succesfully",
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

exports.login = async (req, res) => {
    try {
        let { body } = req;

        let user = await usersCollection.findOne({ email: body.email }).lean().exec();
        errorHandler(user === null, 404, "user not registered");

        let passwordVerification = await bcrypt.compare(body.password, user.password);
        errorHandler(!passwordVerification, 400, "incorrect password");

        var token = jwt.sign(
            {
                id: user._id,
                name: user.name,
                type: "user",
            },
            process.env.JWT_SECRET,
            {
                expiresIn: "5h",
            }
        );

        return res.send({
            message: "Login success",
            token: token,
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

exports.listAllFiles = async (req, res) => {
    try {
        let { user } = req;
        let files = await filesCollection.find({ user_id: user.id }).select(["name", "file"]).lean().exec();

        return res.send({
            message: "data listed",
            data: files.length ? files : [],
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

exports.saveNewFile = async (req, res) => {
    try {
        let { body, user } = req;

        let fileName = (Math.random() + 1).toString(36).substring(2);

        let files = await filesCollection.create({
            name: fileName,
            file: body.file,
            user_id: user.id,
        });

        return res.send({
            message: "file saved succesfully",
        });
    } catch (err) {
        return res.status(err.status).send({
            message: err.message,
        });
    }
};

const mongoose = require("mongoose");

const filesSchema = mongoose.Schema({
    name: { type: String },
    file: { type: String, required: true },
    user_id: { type: String, required: true, ref: "users" },
});

module.exports = mongoose.model("files", filesSchema);
